Set up Vue
==========

so ignore the [tutorial](https://medium.com/the-web-tub/creating-your-first-vue-js-pwa-project-22f7c552fb34),
just checkout the git repo, change into it `cd pwa-vue-app-1` and hit `npm install`. 

to develop, you can use 
`npm run dev` but with this you won't get a progressive webapp. 

This only works by running
* `npm run build`
* `serve -s dist -l 5000`
* `ngrok http 5000`

Now you will see a URL in the terminal which you can open in your mobile device.
Without paying for `ngork`, this URL will stay valid for 8 hours. 
 
Then I copied over my stuff from
[here](https://read.acloud.guru/how-to-add-file-upload-features-to-your-website-with-aws-lambda-and-s3-48bbe9b83eaa)

In order to copy stuff into the `public` folder, look 
[here](https://about.gitlab.com/blog/2016/04/07/gitlab-pages-setup/) on how to write the file `.gitlab-ci.yml`.
There is an example included below. 


Setup Domain With Namecheap
===========================

First, I had to set up the A-Record for APEX (not a subdomain) and a CNAME for the 'www' subdomain,
as described [here](https://gitlab.com/help/user/project/pages/custom_domains_ssl_tls_certification/index.md). 
Use the following values in NameCheap's Advanced DNS (all with TTL automatic)
* A Record: `@`.leicher.eu `35.185.44.232`
* TXT: `_gitlab-pages-verification-code`.leicher.eu `gitlab-pages-verification-code=abcdef1234567890abcdef01234`
* CNAME: `www` `fleicher.gitlab.io.`
* TXT: `_gitlab-pages-verification-code.www`.leicher.eu `gitlab-pages-verification-code=1234567890abcdef01234abcdef`


> :warning: **leicher.eu only**: creates SSL for subdomains automatically.

To serve HTTPS on Gitlab, I used 
[CertBot](https://docs.gitlab.com/ee/user/project/pages/lets_encrypt_for_gitlab_pages.html)
```sudo certbot certonly -a manual -d leicher.eu --email <email-for-expiration@notificati.on>``` 
(not necessary for `-d www.leicher.eu`). The script will tell me: 
- Congratulations! Your certificate and chain have been saved at:
`/etc/letsencrypt/live/leicher.eu/fullchain.pem` (and `/etc/letsencrypt/live/train.leicher.eu/fullchain.pem`)
Your key file has been saved at:
`/etc/letsencrypt/live/leicher.eu/privkey.pem`
Your cert will expire on 2020-02-22. To obtain a new or tweaked
version of this certificate in the future, simply run `certbot`
again. To non-interactively renew **all** of your certificates, run
`certbot renew`.
- Your account credentials have been saved in your CertBot
configuration directory at `/etc/letsencrypt`. You should make a
secure backup of this folder now. This configuration directory will
also contain certificates and private keys obtained by CertBot so
making regular backups of this folder is ideal.


Create a subdomain
==================

This luckily requires fewer steps: 

1. create new Gitlab public project `arlo` (Private ones can't link to external domains somehow). 
2. Add a file called `.gitlab-ci.yml` in the root directory with the following content: 
    ```
    pages:
    stage: deploy
    script:
    - mkdir .public
    - cp -r * .public
    - mv .public public
    artifacts:
    paths:
      - public
    only:
    - master
   ```
3. Go to Settings > Pages and add a new domain: `arlo.leicher.eu`. 
(Note, the address `fleicher.gitlab.io/arlo` is created automagically.)
Here, you will see the Verfication code which looks something like 
`_gitlab-pages-verification-code.arlo.leicher.eu TXT gitlab-pages-verification-code=012345abcdef`

4. Login to Namecheap and add **two** records under **Advanced DNS**: 
  A TXT-Record with Host = `_gitlab-pages-verification-code.arlo`, 
  Value = `gitlab-pages-verification-code=012345abcdef`. 
  And a CNAME-Record with Host = `arlo` and Value = `fleicher.gitlab.io`. 

5. Now finish the verification under Pages in Gitlab. Enable SSL,
  this should only take a few minutes. If it does not, try removing and 
  adding the desired domain (with another verification) again and before 
  hitting save for the first time, toggle the switch for automatic encryption. 


AWS Lambda Serverless
=====================

1. Create your new Lambda in AWS, use a template and the existing client role. 

2. Build (not import) a HTTP gateway (not a REST gateway - that is more complicated)

3. Add, your Lambda Function as Integration. Configure an route for 
    Method = `ANY`, Resource Path = `/arlo`, Integration target = `arlo` (the Lambda's name)
    You can leave the `$default` state which auto-deploys. It will create a URL like: 
    `https://nt4pkc1c53.execute-api.us-east-2.amazonaws.com`. You can invoke your Lambda now by 
    going to `https://nt4pkc1c53.execute-api.us-east-2.amazonaws.com/arlo` in your Browser. 
    
4. To call the Lambda from Gitlab, add a CORS entry with "*" for `Access-Control-Allow-Origin`,
  `Access-Control-Allow-Origin`, `Access-Control-Allow-Methods`, `Access-Control-Expose-Headers`,
  and `Access-Control-Max-Age` as 300 Seconds, and `Access-Control-Allow-Credentials` = No. 

5. You can create the desired files in the directory `/public/`. To do a Lambda integration, 
add the file `/public/index.html` with the following content: 
```
<!DOCTYPE html>
<html>
<head>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script>
$(document).ready(function(){
  $("button").click(function(){
    $("#div1").load("https://nt4pkc1c53.execute-api.us-east-2.amazonaws.com/arlo");
  });
});
</script>
</head>
<body>

<div id="div1"><h2>Let jQuery AJAX Change This Text</h2></div>

<button>Get External Content</button>

</body>
</html>
```

6. Wait until the file has deployed through the pipeline (green tick). Then you can 
    access the file at `http://arlo.leicher.eu/`. After a day, https will also work. 

Create Lambda
=============

To 'install' packages for the Lambda function, use the following commands: 

`mkdir -p lambda_layers/python/lib/python3.7/site-packages`
`pip install arlo -t lambda_layers/python/lib/python3.7/site-packages/ .`

`cd lambda_layers`
`zip -r arlo_layer.zip *`

Go to AWS Lambda Functions `Layers` and create a new layer by uploading the zip file. 
This layer can then be added to the Lambda function. 
